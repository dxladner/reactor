
class Posts extends React.Component{
constructor(){
  super();
  this.state = {
    posts: []
  }
}
componentWillMount(){
  this.getPosts();
}
getPosts() {
      var _this = this;
      $.get('http://localhost/wpvue/wp-json/wp/v2/posts').then(function(response) {
      _this.setState({posts: response}, () => {
        console.log(response);
      })
    });
  }

  render(){
    var post_name = this.state.posts.map(function(post){
        return <tr key={post.id}><td>{post.slug}</td></tr>;
    })
    return (
       <div className="col-md-4">
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">Posts</h3>
          </div>
          <div className="panel-body">
            <table>
              <tbody>

                  {post_name}

              </tbody>
            </table>
          </div>
        </div>
      </div>
      );
    }
  };

  ReactDOM.render(
    <div>
      <div className="row">
        <Posts />
      </div>
    </div>,
      document.getElementById('root')
    );
