<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wpvue
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div id="root"></div>
		</main><!-- #main -->
	</div><!-- #primary -->
	<script type="text/babel">
class Posts extends React.Component{
  constructor(){
    super();
    this.state = {
      posts: []
    }
  }
  componentWillMount(){
    this.getPosts();
  }
  getPosts() {
        var _this = this;
        $.get('http://localhost/wpvue/wp-json/wp/v2/posts?_embed').then(function(response) {
        _this.setState({posts: response}, () => {
          console.log(response);
        })
      });
    }

    render(){
      var posts_data = this.state.posts.map(function(post){
          return (
						<li key={post.id}>
						<h2>{post.title.rendered}</h2>
						<p>Author: {post.author_name} | Posted on {post.date}</p>
						<a href={post.link}><img src={post.featured_image} /></a>
						<p>{post.excerpt.rendered}</p>
						<p>Categories: {post.cat_names} | Tags: {post.tag_names}</p>

						</li>
					);
      })
      return (
				<div>
		      <h1>React Posts</h1>
		      <ul className="list-group posts-list">
		      {posts_data}
		      </ul>
		    </div>
        );
      }
    };

    ReactDOM.render(
      <div>
        <div className="row">
          <Posts />
        </div>
      </div>,
        document.getElementById('root')
      );
</script>
<style>
.posts-list {
	list-style-type: none;
}
</style>
<?php
get_footer();
